import { createStore, compose} from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory, hashHistory } from 'react-router';

//import the root reducer
import rootReducer from './reducers/index';

const store = createStore(rootReducer);

export const history = syncHistoryWithStore(browserHistory, store);


export default store;
