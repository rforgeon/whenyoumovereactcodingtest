import React from 'react';
import { Link } from 'react-router';

const Main = React.createClass({
  render() {
    return(
      <div className='hero'>
        <h1>
          <Link to="/">FRUIT MACHINE</Link>
        </h1>
        {React.cloneElement(this.props.children, this.props)}
      </div>
    )
  }
});


export default Main;
