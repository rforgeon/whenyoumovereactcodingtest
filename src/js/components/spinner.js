import React, { Component } from 'react';

class Spinner extends Component {

  componentDidMount(){
    this.props.init();
    alert("Click 'PLAY' to spin the wheel. If you match 3, you're a winner!");
  }

  checkWinner(){
    //check if spin button has been clicked
    if (this.props.spinner.hasSpun){
      //if so, determine if all collors match to indicate a winner
      if (this.props.spinner.spinners[0].color == this.props.spinner.spinners[1].color && this.props.spinner.spinners[1].color == this.props.spinner.spinners[2].color){
        console.log("WINNER")
        alert("WINNER🎉");
     }
      return
    }
  }

  spinBtnClick(){
    this.props.spin();
    //check if the random colors are a winning spin
    this.checkWinner();
  }


  render() {
    return (
      <div className='hero'>
        <ul style={{color:this.props.spinner.spinners[0].color, margin:+"5"+"px"}}>{this.props.spinner.spinners[0].color}</ul>
        <ul style={{color:this.props.spinner.spinners[1].color, margin:+"5"+"px"}}>{this.props.spinner.spinners[1].color}</ul>
        <ul style={{color:this.props.spinner.spinners[2].color, margin:+"5"+"px"}}>{this.props.spinner.spinners[2].color}</ul>
        <button onClick={this.spinBtnClick.bind(this)}>PLAY</button>
        <p style={{fontSize:+"10"+"px"}}>*Click spin once more if you think you won</p>
      </div>

    )
  }
}

export default Spinner
