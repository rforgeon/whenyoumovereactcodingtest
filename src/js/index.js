import React from 'react';
import {render } from 'react-dom';
import 'babel-polyfill';
import 'stylus/app.styl';

//Import Components
import App from './components/index';
import Spinner from './components/spinner';

//Import react router deps
import {Router, Route, IndexRoute, browserHistory, hashHistory} from 'react-router';
import { Provider } from 'react-redux';
import store, { history } from './store';

const router = (
  <Provider store={store}>
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={Spinner}></IndexRoute>
    </Route>
  </Router>
  </Provider>

)

render(router, document.getElementById('root'));
