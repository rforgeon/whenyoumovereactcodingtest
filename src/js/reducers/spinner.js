var defaultState = {
  hasSpun: false,
  spinners:[{
      color: 'Black'
    },
    {
      color: 'Black'
    },
    {
      color: 'Black'
    },
  ]
}

function spinner(state = defaultState, action){

  switch(action.type){

    case "INIT":
      console.log("INIT")
      return{
        hasSpun: false,
        spinners:[{
            color: 'Black'
          },
          {
            color: 'Black'
          },
          {
            color: 'Black'
          },
        ]
      }

    case "SPIN":
      console.log("SPIN")
      var colors = ["Red","Yellow","Blue","Green"]
      return{
        hasSpun: true,
        spinners:[{
            color: colors[Math.floor(Math.random() * colors.length)]
          },
          {
            color: colors[Math.floor(Math.random() * colors.length)]
          },
          {
            color: colors[Math.floor(Math.random() * colors.length)]
          },
        ]
      }


    default:
      return state;
  }
}

export default spinner;
