import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import spinner from './spinner';

const rootReducer = combineReducers({spinner, routing: routerReducer});

export default rootReducer
